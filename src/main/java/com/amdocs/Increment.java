package com.amdocs;

public class Increment {
	
	private static int counter = 1;
	
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(int input) {
		if (input == 0){
	         counter--;
		}
		else {
			if (input > 1) {
				 counter++;
				}
		}
		return counter;
	}			
}
